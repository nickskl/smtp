#include <fcntl.h>
#include <client-logger.h>
#include <errno.h>
#include <stdio.h>
#include <resolv.h>
#include <memory.h>
#include <stdlib.h>
#include <netdb.h>
#include "smtp_sockets.h"
#include <arpa/inet.h>

void socket_set_nonblocking(int socket, bool is_nonblocking)
{
	int flags = fcntl(socket, F_GETFL);
	if (is_nonblocking)
	{
		fcntl(socket, F_SETFL, flags | O_NONBLOCK);
	}
	else
	{
		fcntl(socket, F_SETFL, flags & ~O_NONBLOCK);
	}
};

int create_local_socket_pair(int *out_sockets)
{
	int result = socketpair(AF_LOCAL, SOCK_DGRAM, 0, out_sockets);
	if (result != 0)
	{
		perror("Error creating socket pair");
		return -1;
	}
	return 0;
};

ssize_t socket_write(int socket, const char *buf, unsigned long len)
{
	ssize_t result = send(socket, buf, len, 0);
	if (result >= 0)
	{
		return result;
	}
	if ((errno = EAGAIN) || (errno == EWOULDBLOCK))
	{
		return 0;
	}
	return -errno;
};

ssize_t socket_read(int socket, char *buf, unsigned long len)
{
	ssize_t result = recv(socket, buf, len, 0);
	if (result >= 0)
	{
		return result;
	}
	if ((errno = EAGAIN) || (errno == EWOULDBLOCK))
	{
		return 0;
	}
	return -errno;
};

int resolvmx(const char *name, char **mxs, int limit) {
	unsigned char response[NS_PACKETSZ];
	ns_msg handle;
	ns_rr rr;
	int mx_index, ns_index, len;
	char dispbuf[4096];

	if ((len = res_search(name, C_IN, T_MX, response, sizeof(response))) < 0)
	{
		/* WARN: res_search failed */
		return -1;
	}

	if (ns_initparse(response, len, &handle) < 0)
	{
		/* WARN: ns_initparse failed */
		return 0;
	}

	len = ns_msg_count(handle, ns_s_an);
	if (len < 0)
		return 0;

	for (mx_index = 0, ns_index = 0;
		 mx_index < limit && ns_index < len;
		 ns_index++) {
		if (ns_parserr(&handle, ns_s_an, ns_index, &rr))
		{
			/* WARN: ns_parserr failed */
			continue;
		}
		ns_sprintrr (&handle, &rr, NULL, NULL, dispbuf, sizeof (dispbuf));
		if (ns_rr_class(rr) == ns_c_in && ns_rr_type(rr) == ns_t_mx) {
			char mxname[MAXDNAME];
			dn_expand(ns_msg_base(handle), ns_msg_base(handle) + ns_msg_size(handle), ns_rr_rdata(rr) + NS_INT16SZ, mxname, sizeof(mxname));
			mxs[mx_index++] = strdup(mxname);
		}
	}

	return mx_index;
}

char* resolve(const char *domain)
{
	char *mx;
	ns_msg handle;
	char *buffer = (char *)malloc(256);

	if (strcmp(domain, "localhost") == 0)
	{
		strcpy(buffer, "127.0.0.1");
		return buffer;
	}
	int num = resolvmx(domain, &mx, 1);

	struct in_addr *addr = NULL;
	if (num > 0)
	{
		struct in_addr **addr_list;
		struct hostent *he = gethostbyname(mx);
		if (!he)
		{
			return NULL;
		}
		addr_list = (struct in_addr **)he->h_addr_list;
		strncpy(buffer, inet_ntoa(*addr_list[0]), 256);
	}
	return buffer;
}

int create_socket()
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	int opt_val = 1;
	socklen_t opt_len = sizeof(opt_val);

	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt_val, opt_len);
	setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, (char *)&opt_val, opt_len);
	socket_set_nonblocking(sock, true);

	return sock;
}
