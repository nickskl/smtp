/** \file path-utils.c
 *  \brief Функции для работы с файловой системой.
 *
 *  В этом файле описаны функции для работы с файловой системой.
 */

#include "path-utils.h"


bool check_if_directory_exists(const char* path)
{
	struct stat sb;
	return (stat(path, &sb) == 0 && S_ISDIR(sb.st_mode));
}


bool check_if_file_exists(const char* path)
{
	struct stat sb;
	return (stat(path, &sb) == 0 && S_ISREG(sb.st_mode));
}


int create_path(const char* path, mode_t mode)
{
	return mkdir(path, mode);
}

int create_subdirectory(const char* path, const char* directory, mode_t mode)
{
	char full_path[0x300];
	strncpy(full_path, path, 0x300);
	strcat(full_path, "/");
	strcat(full_path, directory);
	return create_path(full_path, mode);
}

bool get_directory_listing(const char *path, struct linked_list **listing)
{
	DIR* dp = NULL;
	struct dirent *ep = NULL;
	char file_path[0x300];
	char *buffer_ptr = NULL;
	uint32_t max_filename_len = sizeof(file_path) - sizeof(path) - 1;
	
	if (max_filename_len <= 0)
	{
		perror("Path name is too long");
		return false;
	}
	
	if ((listing == NULL)||(*listing != NULL))
	{
		perror("Listing variable must be a pointer to NULL");
		return false;
	}
	
	dp = opendir(path);
	
	*listing = linked_list_create();

	if (dp != NULL)
	{
		strncpy(file_path, path, sizeof(file_path));
		strcat(file_path, "/");
		size_t  len = strlen(file_path);
		while ((ep = readdir(dp)) != NULL)
		{
			if ((strcmp(ep->d_name, ".") != 0) && (strcmp(ep->d_name, "..") != 0))
			{
				strncpy(&file_path[len], ep->d_name, max_filename_len);

				if (check_if_file_exists(file_path ))
				{
					linked_list_push(*listing, file_path , (uint32_t)strlen(file_path)+1);
				}
			}
		}
		closedir(dp);
		return true;
	}
	perror("Unable to open directory");
	return false;
}

int free_listing(struct linked_list* listing)
{
	linked_list_free(listing);
    return 0;
}

char * get_any_file(const char *dir)
{
	DIR *dp = NULL;
	struct dirent *ep = NULL;
	char *result = NULL;

	dp = opendir(dir);

	if (dp != NULL)
	{
		while ((ep = readdir(dp)) != NULL)
		{
			if ((strcmp(ep->d_name, ".") != 0) && (strcmp(ep->d_name, "..") != 0))
			{
				result = (char *) malloc(strlen(ep->d_name) + 1);
				strncpy(result, ep->d_name, strlen(ep->d_name));
				result[strlen(ep->d_name)]=0;
				break;
			}
		}
		closedir(dp);
	}
	return result;
}

bool move_all_files(const char *old_dir, const char *new_dir)
{
	DIR *dp = NULL;
	struct dirent *ep = NULL;
	char *old_path = malloc(0x300);
	char *new_path = malloc(0x300);

	strcpy(old_path, old_dir);
	strcat(old_path, "/");
	size_t old_path_len = strlen(old_dir)+1;
	old_path[old_path_len] = 0;

	strcpy(new_path, new_dir);
	strcat(new_path, "/");
	size_t new_path_len = strlen(new_dir)+1;
	new_path[new_path_len] = 0;

	dp = opendir(old_dir);

	if (dp != NULL)
	{
		while ((ep = readdir(dp)) != NULL)
		{
			if ((strcmp(ep->d_name, ".") != 0) && (strcmp(ep->d_name, "..") != 0))
			{
				strcpy(old_path + old_path_len, ep->d_name);
				//old_path[old_path_len + strlen(ep->d_name)] = 0;
				strcpy(new_path + new_path_len, ep->d_name);
				//new_path[new_path_len + strlen(ep->d_name)] = 0;
				if (rename(old_path, new_path) != 0)
				{
					printf("Failed to move %s to %s", old_path, new_path);
				}

			}
		}
		closedir(dp);
	}
	free(old_path);
	free(new_path);
	return true;
}
