
#include <path-utils.h>
#include "mail-struct.h"

struct mail_struct *get_next_mail_from_dir(const char *dir)
{
    char *filename = get_any_file(dir);
    struct mail_struct *result = NULL;
    if (filename != NULL)
    {
        result = (struct mail_struct*)malloc(sizeof(struct mail_struct));
        load_file_to_mail_struct(dir, filename, result);
        free(filename);
    }
    return result;

}

int load_file_to_mail_struct(const char* path, const char *filename, struct mail_struct *out_mail_struct)
{
    char full_path[0x200];
    strncpy(full_path, path, 0x200);
    strcat(full_path, "/");
    strcat(full_path, filename);
    out_mail_struct->from = NULL;
    out_mail_struct->to = NULL;

    FILE *file = fopen(full_path, "r");
    char *buffer = (char *) malloc(MAIL_CHUNK_SIZE);
    char *from_string_ptr = NULL;
    char *to_string_ptr = NULL;
    size_t total_len = 0;
    while (fgets(buffer, MAIL_CHUNK_SIZE, file) != NULL) {
        from_string_ptr = strstr(buffer, MAIL_FROM_STRING);
        if ((from_string_ptr != NULL)&&(out_mail_struct->from == NULL))
        {
            out_mail_struct->from = malloc(strlen(buffer));
            strcpy(out_mail_struct->from, buffer + strlen(MAIL_FROM_STRING));
        }
        to_string_ptr = strstr(buffer, MAIL_TO_STRING);
        if ((to_string_ptr != NULL)&&(out_mail_struct->to == NULL))
        {
            out_mail_struct->to = malloc(strlen(buffer));
            strcpy(out_mail_struct->to, buffer + strlen(MAIL_TO_STRING));
        }
        total_len += strlen(buffer);
    }
    if (out_mail_struct->from != 0)
    {
        size_t len = strlen(out_mail_struct->from);
        while ((len > 0) && ((out_mail_struct->from[len - 1] >= 'z') || (out_mail_struct->from[len - 1] <= 'a')))
        {
            out_mail_struct->from[len-1] = 0;
            len--;
        }
    }
    if (out_mail_struct->to != 0)
    {
        size_t len = strlen(out_mail_struct->to);
        while ((len > 0) && ((out_mail_struct->to[len - 1] >= 'z') || (out_mail_struct->to[len - 1] <= 'a')))
        {
            out_mail_struct->to[len-1] = 0;
            len--;
        }
    }
    out_mail_struct->file_size = total_len;
    out_mail_struct->filename = (char *)malloc(strlen(filename) + 1);
    strcpy(out_mail_struct->filename, filename);
    out_mail_struct->filename[strlen(filename)] = 0;
    fclose(file);
    free(buffer);

    return 0;
}

int free_mail_struct(struct mail_struct *mail_struct)
{
    free(mail_struct->to);
    free(mail_struct->from);
    free(mail_struct->filename);
    return 0;
}

