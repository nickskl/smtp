#ifndef __MAIL_STRUCT_H__
#define __MAIL_STRUCT_H__

#include <bits/types/FILE.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define MAIL_FROM_STRING "From: "
#define MAIL_TO_STRING "To: "

#define MAIL_CHUNK_SIZE 256

struct mail_struct
{
	char *filename;
	char *from;
	char *to;
	size_t file_size;
};

int load_file_to_mail_struct(const char* path, const char *filename, struct mail_struct *out_mail_struct);
int free_mail_struct(struct mail_struct *mail_struct);
struct mail_struct *get_next_mail_from_dir(const char *dir);

#endif
