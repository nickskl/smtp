#ifndef __CLIENT_COMMANDS_H__
#define __CLIENT_COMMANDS_H__

#include <glob.h>
#include <wait.h>
#include "client-context.h"

enum process_control_commands
{
    SMTP_CLIENT_TASK,
    SMTP_CLIENT_LOG_PRINT,
    SMTP_CLIENT_PROCESS_STOP
};

#define CLIENT_COMMAND_HELO "HELO localhost\r\n"
#define CLIENT_COMMAND_EHLO "EHLO localhost\r\n"
#define CLIENT_COMMAND_MAIL_FROM "MAIL FROM:"
#define CLIENT_COMMAND_RCPT_TO "RCPT TO:"
#define CLIENT_COMMAND_DATA "DATA\r\n"
#define CLIENT_COMMAND_END_OF_DATA ".\r\n"
#define CLIENT_COMMAND_QUIT "QUIT\r\n"
#define CLIENT_COMMAND_RESET "RSET\r\n"

struct client_process_command
{
    enum process_control_commands type;
};

int send_terminate_to_worker(struct smtp_client_worker_context *worker_ctx);
int send_task_to_worker(struct smtp_client_worker_context *worker_ctx, const void *task, size_t task_size);
int read_command(int socket, struct client_process_command *out_command);

#endif