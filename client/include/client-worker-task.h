#ifndef __CLIENT_WORKER_TASK_H__
#define __CLIENT_WORKER_TASK_H__

#include "linked-list.h"
#include "client-fsm.h"
#include <stdlib.h>
#include <string.h>
#include <mail-struct.h>


enum task_state
{
	TASK_WAINTING_IN_QUEUE,
	TASK_RUNNING,
	TASK_FINISHED
};

struct worker_task
{
	char *domain;
	char *directory;
	uint32_t number_of_reties;
	int socket;
	bool expect_reads_from_socket;
	te_client_fsm_state smtp_fsm_state;
	enum task_state task_state;
	struct mail_struct *mail;

};

struct linked_list *create_new_task_queue();
struct worker_task *get_next_task(struct linked_list *tasks);
void add_task_to_queue(struct linked_list *tasks, struct worker_task* task);
void remove_all_finished_tasks_from_queue(struct linked_list *tasks, const char *base_dir);
struct worker_task *get_task_from_node(struct linked_list_node *node);
void free_task_queue(struct linked_list *tasks, const char *base_mail_dir);

#endif

