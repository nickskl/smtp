#include <smtp_sockets.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "client-worker.h"


int receive_new_command(struct smtp_client_worker_context *ctx, int socket)
{
    struct client_process_command command;
    ssize_t result = recv(socket, &command, sizeof(command), 0);
    if (result < 0)
    {
        log_print(ctx->name, "recv() command from main task failed");
        return result;
    }
    if (command.type == SMTP_CLIENT_PROCESS_STOP)
    {
        log_print(ctx->name, "received terminate command from main process");
        ctx->is_running = false;
    }
    if (command.type == SMTP_CLIENT_TASK)
    {
        log_print(ctx->name, "received task command from main process");
        uint32_t len;
        result = recv(socket, &len, sizeof(len), 0);
        if (result < 0)
        {
            log_print(ctx->name, "recv() command from main task failed");
            return result;
        }
        char *domain = (char *)malloc(len+1);
        result = recv(socket, domain, len, 0);
        if (result < 0)
        {
            log_print(ctx->name, "recv() command from main task failed");
            return result;
        }
        struct worker_task *new_task = malloc(sizeof(struct worker_task));
        new_task->smtp_fsm_state = CLIENT_FSM_ST_INIT;
        new_task->domain = (char *)malloc(len);
        strcpy(new_task->domain, domain);
        char path[150];
        snprintf(path, 150, "%s/%d/%s", ctx->process_dir, getpid(), domain);
        new_task->directory = (char *)malloc(strlen(path)+1);
        strcpy(new_task->directory, path);
        new_task->expect_reads_from_socket = true;
        new_task->task_state = TASK_WAINTING_IN_QUEUE;
        new_task->number_of_reties = 0;
        new_task->socket = 0;
        new_task->mail = get_next_mail_from_dir(new_task->directory);

        add_task_to_queue(ctx->task_queue, new_task);
        free(new_task);
        free(domain);
    }
    return 0;
}

int state_handler_create_socket(struct smtp_client_worker_context *ctx, struct hashtable *sockets,
                                fd_set *read_fds, fd_set *write_fds, struct worker_task *task, int *max_fds)
{
    void * socket_for_domain = hashtable_get(sockets, task->domain, strlen(task->domain));
    if (socket_for_domain == NULL)
    {
        task->socket = create_socket();
        hashtable_put(sockets, task->domain, strlen(task->domain), &task->socket, sizeof(task->socket));
        FD_SET(task->socket, read_fds);
        FD_SET(task->socket, write_fds);
        if (task->socket > *max_fds)
        {
            *max_fds = task->socket;
        }
    }
    else
    {
        task->socket = *(int *)socket_for_domain;
    }
    task->smtp_fsm_state = CLIENT_FSM_ST_STATE_CREATE_SOCKET;
    return 0;
}

int state_handler_connect(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    char* mx_ip = resolve(current_task->domain);
    log_print(ctx->name, "MX IP for domain %s is %s", current_task->domain, mx_ip);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SERVER_PORT);
    inet_pton(AF_INET, mx_ip, &addr.sin_addr);
    int result = connect(current_task->socket, (struct sockaddr*)&addr, sizeof(addr));
    if(result < 0)
    {
        log_print(ctx->name, "Failed to connect to server");
        free(mx_ip);
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_FREE_SOCKET;
        return result;
    }
    log_print(ctx->name, "Connected to %s", mx_ip);
    free(mx_ip);
    current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_CONNECTED;
    return 0;
}

int state_handler_read_greeting(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    char buffer[WORKER_BUFFER_SIZE];
    ssize_t len = socket_read(current_task->socket, buffer, WORKER_BUFFER_SIZE);
    if (len < 0)
    {
        log_print(ctx->name, "Reading from socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;
    }
    buffer[len]=0;
    struct smtp_server_response_match match = server_response_parse_message(buffer, 0);
    if (match.response == SERVER_CONNECT)
    {
        log_print(ctx->name, "Got server greeting message: %s", buffer);
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_RECEIVE_SMTP_GREETING;
    }
    else
    {
        log_print(ctx->name, "Unknown message received");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_ERROR;
    }

    return 0;
}

int state_handler_read_ehlo_response(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    char buffer[WORKER_BUFFER_SIZE];
    ssize_t len = socket_read(current_task->socket, buffer, WORKER_BUFFER_SIZE);
    if (len < 0)
    {
        log_print(ctx->name, "Reading from socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    buffer[len]=0;
    struct smtp_server_response_match match = server_response_parse_message(buffer, 0);
    if (match.response == SERVER_EHLO_NOT_SUPPORTED)
    {
        log_print(ctx->name, "EHLO not supported: %s", buffer);
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_FALLBACK_TO_HELO;
    }
    else if (match.response == SERVER_EHLO_OK_MULTILINE)
    {
        log_print(ctx->name, "Multiline EHLO response");
    }
    else if (match.response == SERVER_OK)
    {
        log_print(ctx->name, "Got OK");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_RECEIVE_HELO_RESPONSE;
    }
    else
    {
        log_print(ctx->name, "Unknown message received");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_ERROR;
    }
    return 0;
}

int state_handler_read_mail_from_response(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    char buffer[WORKER_BUFFER_SIZE];
    ssize_t len = socket_read(current_task->socket, buffer, WORKER_BUFFER_SIZE);
    if (len < 0)
    {
        log_print(ctx->name, "Reading from socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    buffer[len]=0;
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    struct smtp_server_response_match match = server_response_parse_message(buffer, 0);
    if (match.response == SERVER_OK)
    {
        log_print(ctx->name, "Got OK");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_RECEIVE_MAIL_FROM_RESPONSE;
    }
    else
    {
        log_print(ctx->name, "Unknown message received");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_ERROR;
    }
    return 0;
}

int state_handler_read_rcpt_to_response(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    char buffer[WORKER_BUFFER_SIZE];
    ssize_t len = socket_read(current_task->socket, buffer, WORKER_BUFFER_SIZE);
    if (len < 0)
    {
        log_print(ctx->name, "Reading from socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    buffer[len]=0;
    struct smtp_server_response_match match = server_response_parse_message(buffer, 0);
    if (match.response == SERVER_OK)
    {
        log_print(ctx->name, "Got OK");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_RECEIVE_RCPT_TO_RESPONSE;
    }
    else
    {
        log_print(ctx->name, "Unknown message received");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_ERROR;
    }
    return 0;
}


int state_handler_read_data_response(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    char buffer[WORKER_BUFFER_SIZE];
    ssize_t len = socket_read(current_task->socket, buffer, WORKER_BUFFER_SIZE);
    if (len < 0)
    {
        log_print(ctx->name, "Reading from socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    buffer[len]=0;
    struct smtp_server_response_match match = server_response_parse_message(buffer, 0);
    if (match.response == SERVER_DATA_OK)
    {
        log_print(ctx->name, "Got Data OK");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_RECEIVE_DATA_RESPONSE;
    }
    else
    {
        log_print(ctx->name, "Unknown message received");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_ERROR;
    }
    return 0;
}

int state_handler_read_message_body_response(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    char buffer[WORKER_BUFFER_SIZE];
    ssize_t len = socket_read(current_task->socket, buffer, WORKER_BUFFER_SIZE);
    if (len < 0)
    {
        log_print(ctx->name, "Reading from socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    buffer[len]=0;
    struct smtp_server_response_match match = server_response_parse_message(buffer, 0);
    if (match.response == SERVER_OK)
    {
        log_print(ctx->name, "Got OK on message body transfer");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_RECEIVE_MESSAGE_BODY_RESPONSE;
    }
    else
    {
        log_print(ctx->name, "Unknown message received");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_ERROR;
    }
    return 0;
}

int state_handler_read_quit_response(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    char buffer[WORKER_BUFFER_SIZE];
    ssize_t len = socket_read(current_task->socket, buffer, WORKER_BUFFER_SIZE);
    if (len < 0)
    {
        log_print(ctx->name, "Reading from socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_FREE_SOCKET;
        }
        return len;

    }
    buffer[len]=0;
    struct smtp_server_response_match match = server_response_parse_message(buffer, 0);
    if (match.response == SERVER_QUIT_OK)
    {
        log_print(ctx->name, "Got quit OK");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_FREE_SOCKET;
    }
    else
    {
        log_print(ctx->name, "Unknown message received");
        current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_ERROR;
    }
    return 0;
}

int read_from_server(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    switch (current_task->smtp_fsm_state)
    {
        case CLIENT_FSM_ST_STATE_CONNECTED:
            state_handler_read_greeting(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_RECEIVE_EHLO_MULTILINE_RESPONSE:
            state_handler_read_ehlo_response(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_SEND_EHLO:
            state_handler_read_ehlo_response(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_SEND_HELO:
            state_handler_read_ehlo_response(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_SEND_MAIL_FROM:
            state_handler_read_mail_from_response(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_SEND_RCPT_TO:
            state_handler_read_rcpt_to_response(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_SEND_DATA:
            state_handler_read_data_response(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_SEND_MESSAGE_BODY:
            state_handler_read_message_body_response(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_SEND_QUIT:
            state_handler_read_quit_response(ctx, current_task);
            break;
        default:
            return -1;
    }
    return 0;
}

int state_handler_send_ehlo(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }
    const char *buffer = CLIENT_COMMAND_EHLO;
    ssize_t len = socket_write(current_task->socket, buffer, strlen(buffer));
    if (len < 0)
    {
        log_print(ctx->name, "Writing to socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    log_print(ctx->name, "EHLO sent");
    current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_SEND_EHLO;
    return 0;
};

int state_handler_send_mail_from(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }
    char buffer[WORKER_BUFFER_SIZE];
    strcpy(buffer, CLIENT_COMMAND_MAIL_FROM);
    strcat(buffer, current_task->mail->from);
    strcat(buffer, "\r\n");

    ssize_t len = socket_write(current_task->socket, buffer, strlen(buffer));
    if (len < 0)
    {
        log_print(ctx->name, "Writing to socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    log_print(ctx->name, "MAIL FROM sent");
    current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_SEND_MAIL_FROM;
    return 0;
};

int state_handler_send_rcpt_to(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }
    char buffer[WORKER_BUFFER_SIZE];
    strcpy(buffer, CLIENT_COMMAND_RCPT_TO);
    strcat(buffer, current_task->mail->to);
    strcat(buffer, "\r\n");

    ssize_t len = socket_write(current_task->socket, buffer, strlen(buffer));
    if (len < 0)
    {
        log_print(ctx->name, "Writing to socket failed");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        return len;
    }
    if (len == 0)
    {
        log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }
        return len;
    }
    log_print(ctx->name, "RCPT TO sent");
    current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_SEND_RCPT_TO;
    return 0;
};

int state_handler_send_data(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }
    const char *buffer = CLIENT_COMMAND_DATA;

    ssize_t len = socket_write(current_task->socket, buffer, strlen(buffer));
    if (len < 0)
    {
log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    log_print(ctx->name, "DATA sent");
    current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_SEND_DATA;
    return 0;
};


int state_handler_send_message_body(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }
    char *buffer = (char *)calloc(current_task->mail->file_size + strlen(CLIENT_COMMAND_END_OF_DATA)+1, 1);
    char path_to_file[0x300];
    strncpy(path_to_file, current_task->directory, 0x300);
    strcat(path_to_file, "/");
    strcat(path_to_file, current_task->mail->filename);
    FILE *f = fopen(path_to_file, "r");
    fread(buffer, current_task->mail->file_size, 1, f);
    fclose(f);

    strcat(buffer, CLIENT_COMMAND_END_OF_DATA);
    buffer[current_task->mail->file_size + strlen(CLIENT_COMMAND_END_OF_DATA)] = 0;
    int len = socket_write(current_task->socket, buffer, current_task->mail->file_size);
    if (len < 0)
    {
        log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    log_print(ctx->name, "Message body sent");
    current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_READY_TO_SEND_NEXT_MESSAGE;
    return 0;

};

int state_handler_send_helo(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }
    const char *buffer = CLIENT_COMMAND_HELO;
    ssize_t len = socket_write(current_task->socket, buffer, strlen(buffer));
    if (len < 0)
    {
        log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    log_print(ctx->name, "HELO sent");
    current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_SEND_HELO;
    return 0;

};

int state_handler_send_quit(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }
    const char *buffer = CLIENT_COMMAND_QUIT;
    ssize_t len = socket_write(current_task->socket, buffer, strlen(buffer));
    if (len < 0)
    {
        log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }
        return len;
    }
    if (len == 0)
    {
                log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
        if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
        {
            current_task->number_of_reties++;
            sleep(ctx->mail_retry_wait_time);
        }
        else
        {
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
        }      
        return len;

    }
    log_print(ctx->name, "QUIT sent");
    current_task->smtp_fsm_state= CLIENT_FSM_ST_STATE_FREE_SOCKET;
    return 0;

};


int state_handler_invalid_state(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    if (current_task == NULL)
    {
        log_print(ctx->name, "Received NULL as a task pointer");
        return -1;
    }

    if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
    {
        const char *buffer = CLIENT_COMMAND_EHLO;
        ssize_t len = socket_write(current_task->socket, buffer, strlen(buffer));
        if (len < 0)
        {
            log_print(ctx->name, "Error");
            current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_ERROR;
            return len;
        }

        if (len == 0)
        {
            log_print(ctx->name, "EWOULDBLOCK or EAGAIN");
            if (current_task->number_of_reties < MAX_NUMBER_OF_RETRIES)
            {
                current_task->number_of_reties++;
                sleep(ctx->mail_retry_wait_time);
            }
            else
            {
                char *old_path = NULL;
                char *new_path = NULL;

                int len = snprintf(old_path, 0, "%s/%s", current_task->directory, current_task->mail->filename);
                old_path = (char *)malloc(len+1);
                snprintf(old_path, len+1, "%s/%s", current_task->directory,current_task->mail->filename);
                old_path[len] = 0;

                len = snprintf(new_path, 0, "%s/%s", ctx->sent_dir,current_task->mail->filename);
                new_path = (char *)malloc(len+1);
                snprintf(new_path, len+1, "%s/%s", ctx->sent_dir,current_task->mail->filename);
                new_path[len] = 0;

                if (rename(old_path, new_path) != 0)
                {
                    log_print(ctx->name, "Failed to move %s to %s", old_path, new_path);
                }
                free(old_path);
                free(new_path);


                current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_READY_TO_SEND_NEXT_MESSAGE;
            }
            return len;
        }
        log_print(ctx->name, "RSET sent");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_RECEIVE_SMTP_GREETING;
    }
    else
    {
        log_print(ctx->name, "Failed to send a mail and retry count reached maximum");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_SEND_QUIT;
    }

    return 0;

};

int state_handler_error(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    return state_handler_invalid_state(ctx, current_task);
};


int write_to_server(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    switch (current_task->smtp_fsm_state)
    {
        case CLIENT_FSM_ST_STATE_RECEIVE_SMTP_GREETING:
            state_handler_send_ehlo(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_RECEIVE_EHLO_RESPONSE:
            state_handler_send_mail_from(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_RECEIVE_HELO_RESPONSE:
            state_handler_send_mail_from(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_RECEIVE_MAIL_FROM_RESPONSE:
            state_handler_send_rcpt_to(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_RECEIVE_RCPT_TO_RESPONSE:
            state_handler_send_data(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_RECEIVE_DATA_RESPONSE:
            state_handler_send_message_body(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_FALLBACK_TO_HELO:
            state_handler_send_helo(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_FINISH_SENDING_MAIL:
            state_handler_send_quit(ctx, current_task);
            break;
        case CLIENT_FSM_ST_INVALID:
            state_handler_invalid_state(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_ERROR:
            state_handler_error(ctx, current_task);
            break;
        default:
            return -1;
    }
    return 0;
}

int state_handler_next_message(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    char *old_path = NULL;
    char *new_path = NULL;

    int len = snprintf(old_path, 0, "%s/%s", current_task->directory,current_task->mail->filename);
    old_path = (char *)malloc((size_t)len+1);
    snprintf(old_path, (size_t)len+1, "%s/%s", current_task->directory,current_task->mail->filename);
    old_path[len] = 0;

    len = snprintf(new_path, 0, "%s/%s", ctx->sent_dir,current_task->mail->filename);
    new_path = (char *)malloc((size_t)len+1);
    snprintf(new_path, (size_t)len+1, "%s/%s", ctx->sent_dir,current_task->mail->filename);
    new_path[len] = 0;

    if (rename(old_path, new_path) != 0)
    {
        log_print(ctx->name, "Failed to move %s to %s", old_path, new_path);
    }
    free(old_path);
    free(new_path);

    if (current_task->mail != NULL)
    {
        free_mail_struct(current_task->mail);
    }

    struct mail_struct* mail = get_next_mail_from_dir(current_task->directory);
    current_task->mail = mail;
    if (mail == NULL)
    {
        log_print(ctx->name, "Task finished");
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_FINISH_SENDING_MAIL;
    }
    else
    {
        log_print(ctx->name, "Sending next mail (%s)", mail->filename);
        current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_RECEIVE_EHLO_RESPONSE;
    }

    return 0;
};

int state_handler_close_socket(struct smtp_client_worker_context* ctx, struct worker_task *current_task,
        fd_set *read_fds, fd_set *write_fds)
{
    log_print(ctx->name, "Closing socket");
    FD_CLR(current_task->socket, read_fds);
    FD_CLR(current_task->socket, write_fds);
    close(current_task->socket);
    current_task->task_state = TASK_FINISHED;
    current_task->smtp_fsm_state = CLIENT_FSM_ST_DONE;
    return 0;
};

int state_handler_receive_message_body_response(struct smtp_client_worker_context* ctx, struct worker_task *current_task)
{
    log_print(ctx->name, "Ready to send next message");
    current_task->smtp_fsm_state = CLIENT_FSM_ST_STATE_READY_TO_SEND_NEXT_MESSAGE;

    return 0;
};
int handle_special_states(struct smtp_client_worker_context *ctx, struct worker_task *current_task,
                          struct hashtable *sockets, fd_set *read_fds, fd_set *write_fds, int *max_fds)
{
    switch (current_task->smtp_fsm_state)
    {
        case CLIENT_FSM_ST_INIT:
            state_handler_create_socket(ctx, sockets, read_fds, write_fds, current_task, max_fds);
            break;
        case CLIENT_FSM_ST_STATE_READY_TO_SEND_NEXT_MESSAGE:
            state_handler_next_message(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_CREATE_SOCKET:
            state_handler_connect(ctx, current_task);
            break;
        case CLIENT_FSM_ST_STATE_FREE_SOCKET:
            state_handler_close_socket(ctx, current_task, read_fds, write_fds);
            break;
        case CLIENT_FSM_ST_STATE_RECEIVE_MESSAGE_BODY_RESPONSE:
            state_handler_receive_message_body_response(ctx, current_task);
            break;
        default:
            return -1;
    }
    return 0;
}

int worker_process_run(struct smtp_client_worker_context* ctx)
{
    server_response_parser_init();
    size_t len = strlen(WORKER_PROCESS_NAME) + 20;
    ctx->name = (char *)malloc(len);
    snprintf(ctx->name, len, WORKER_PROCESS_NAME, getpid());
    fd_set read_fds, write_fds;
    fd_set current_read_fds, current_write_fds;
    log_print(ctx->name, "starting worker loop");
    ctx->task_queue = create_new_task_queue();
    struct hashtable * server_sockets = outgoing_mail_dictionary_create();

    FD_ZERO(&read_fds);
    FD_ZERO(&current_read_fds);
    FD_ZERO(&write_fds);
    FD_ZERO(&current_write_fds);
    FD_SET(ctx->master_socket, &read_fds);

    int max_fds = ctx->master_socket;
    struct timeval timeout =
    {
        .tv_sec = 60,
        .tv_usec = 0
    };
    while(ctx->is_running)
    {
        memcpy(&current_read_fds, &read_fds, sizeof(read_fds));
        memcpy(&current_write_fds, &write_fds, sizeof(read_fds));
        FD_SET(ctx->master_socket, &current_read_fds);

        int result = select(max_fds + 1, &current_read_fds, &current_write_fds, NULL, NULL);

        switch (result)
        {
            case -1:
                log_print(ctx->name, "select() failed");
                ctx->is_running = false;
                break;
            default:

                if (FD_ISSET(ctx->master_socket, &current_read_fds))
                {
                    receive_new_command(ctx, ctx->master_socket);
                }

                struct linked_list_node *current_task_node = ctx->task_queue->head;

                while(current_task_node != NULL)
                {
                    struct worker_task * current_task = get_task_from_node(current_task_node);
                    if (handle_special_states(ctx, current_task, server_sockets, &read_fds, &write_fds, &max_fds) < 0)
                    {
                        if (FD_ISSET(current_task->socket, &current_read_fds))
                        {
                            read_from_server(ctx, current_task);
                        }
                        if (FD_ISSET(current_task->socket, &current_write_fds))
                        {
                            write_to_server(ctx, current_task);
                        }
                    }
                    else
                    {
                        continue;
                    }
                    current_task_node = current_task_node->next;
                }
                remove_all_finished_tasks_from_queue(ctx->task_queue, ctx->failed_dir);
        }
    }
    log_print(ctx->name, "Freeing memory");
    server_response_parser_free();
    free(ctx->name);
    free_task_queue(ctx->task_queue, ctx->outmail_dir);
    hashtable_free(server_sockets);

    exit(0);
}
