#include <sys/socket.h>
#include <zconf.h>
#include <smtp_sockets.h>
#include "client-worker-commands.h"

int send_terminate_to_worker(struct smtp_client_worker_context *worker_ctx)
{
    struct client_process_command command =
    {
        .type = SMTP_CLIENT_PROCESS_STOP
    };

    send(worker_ctx->worker_socket, &command, sizeof(command), 0);
    close(worker_ctx->worker_socket);
    return 0;
}

int send_task_to_worker(struct smtp_client_worker_context *worker_ctx, const void *task, size_t task_size)
{
    struct client_process_command command =
     {
        .type = SMTP_CLIENT_TASK
     };

    ssize_t result = socket_write(worker_ctx->worker_socket, (char *)&command, sizeof(command));
    if (result <= 0)
    {
        perror("Sending command to worker failed");
    }
    result = socket_write(worker_ctx->worker_socket, (char *)&task_size, sizeof(task_size));
    if (result <= 0)
    {
        perror("Sending command to worker failed");
    }
    result = socket_write(worker_ctx->worker_socket, (char *)task, task_size);
    if (result <= 0)
    {
        perror("Sending command to worker failed");
    }
    return 0;
}

int read_command(int socket, struct client_process_command *out_command)
{
    recv(socket, out_command, sizeof(struct client_process_command), 0);
    return 0;
}
