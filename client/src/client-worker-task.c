#include <path-utils.h>
#include "client-worker-task.h"

struct linked_list *create_new_task_queue()
{
	struct linked_list *new_task_queue = linked_list_create();

	return new_task_queue;
}

struct worker_task *get_next_task(struct linked_list *tasks)
{
	struct worker_task *next_task = (struct worker_task *)linked_list_pop(tasks);

	return next_task;
}

struct worker_task *get_task_from_node(struct linked_list_node *node)
{
	if (node == NULL)
	{
		return NULL;
	}
	return (struct worker_task *)(node->data);
}

void add_task_to_queue(struct linked_list *tasks, struct worker_task* task)
{
	linked_list_push(tasks, task, sizeof(struct worker_task));
}

void remove_all_finished_tasks_from_queue(struct linked_list *tasks, const char *base_dir)
{
	struct linked_list_node *current_node = tasks->head;
	uint32_t index = 0;
	while(current_node != NULL)
	{
		if (((struct worker_task *)current_node->data)->task_state == TASK_FINISHED)
		{
			struct worker_task* task = current_node->data;
			current_node = current_node->next;
			move_all_files(task->directory, base_dir);
			free(task->domain);
			free(task->directory);
			if (task->mail != NULL)
			{
				free_mail_struct(task->mail);
			}
			linked_list_remove(tasks, index);

		}
		else
		{
			index++;
			current_node = current_node->next;
		}
	}
}

void free_task_queue(struct linked_list *tasks, const char *base_mail_dir)
{
	struct linked_list_node *current_node = tasks->head;
	uint32_t index = 0;
	while(current_node != NULL)
	{
		struct worker_task* task = current_node->data;
		current_node = current_node->next;
		move_all_files(task->directory, base_mail_dir);
		free(task->domain);
		free(task->directory);
		if (task->mail != NULL)
		{
			free_mail_struct(task->mail);
		}
		linked_list_remove(tasks, index);
		index++;
	}
	free(tasks);
}